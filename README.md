This program was written to practise Spring/Spring Boot and Postgresql.
This is the backend for the application which interacts with a server and database.

**Assignment
Create a database and access it**

About Project 

Models, Repositories, and the Application file, which executes the program, are independent from the code.
All of the data types utilized in the aforementioned queries may be found in the Models folder. All of the code needed to execute database queries is located in the Repositories folder.  The code can function without dependence on a particular database thanks to the repository pattern. Different branches for different methods can be found in our gitlab.

****How to use:****

To try out this program just follow the steps bellow.

1. Install [ Postgres and pgAdmin4](https://www.pgadmin.org/download/)

2. Implement the chinook database.

3. Clone this repository and configure the database with the cloned backend.

****Queries**:**

Get a list of all customers
Get a specific customer by ID
Get a specific customer by name
Get a page of customers by setting an offset and a limit
Add a new customer
Update a customer
Get total amount of customers per country
Get the most customer spender
Get the most popular genre for a customer

**Pipeline**

MVM Install globally 

We can run it by typing MVM install

And in the project we are using our own runner in the gitlab

**Maintainers**

@DanSho
@lynxxxxx
@Enwynn 


