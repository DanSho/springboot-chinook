package com.example.springchinook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class SpringChinookApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringChinookApplication.class, args);
	}

}
