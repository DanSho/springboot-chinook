package com.example.springchinook.model;

import lombok.Value;

/**
 * Record classes are a special kind of class. which basically has a concise for defining immutable data-only classes. Which is basically for holding records returned from a database query
  */

public record CustomerCountry(String countryName, int totalCustomers)  {

}
