package com.example.springchinook.model;

/**
 * Record classes are a special kind of class. which basically has a concise for defining immutable data-only classes. Which is basically for holding records returned from a database query
 * @param customerId
 * @param firstname
 * @param lastname
 * @param total
 */
public record CustomerSpender(int customerId, String firstname, String lastname, double total) {
}
