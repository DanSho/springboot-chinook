package com.example.springchinook.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 *  Record classes are a special kind of class. which basically has a concise for defining immutable data-only classes. Which is basically for holding records returned from a database query
 * @param id
 * @param firstName
 * @param lastName
 * @param country
 * @param postalCode
 * @param phone
 * @param email
 */
public record Customer(Integer id, String firstName, String lastName,
                       String country, String postalCode, String phone, String email) {



}
