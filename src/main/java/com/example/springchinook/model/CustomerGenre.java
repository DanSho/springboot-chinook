package com.example.springchinook.model;

/**
 * Record classes are a special kind of class. which basically has a concise for defining immutable data-only classes. Which is basically for holding records returned from a database query
 * @param genre_Name
 * @param firstname
 * @param most_popular_genre
 */
public record CustomerGenre (String genre_Name, String firstname, int most_popular_genre) {
}
