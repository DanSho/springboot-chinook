package com.example.springchinook.repository;

import com.example.springchinook.model.CustomerCountry;
import com.example.springchinook.model.CustomerGenre;
import com.example.springchinook.model.CustomerSpender;

import java.util.List;

/**
 * CRUDrepository  which provides CRUD functionality
 * @param <T>
 * @param <ID>
 */
public interface CrudRepository<T,ID> {
    /**
     * List all customer from customer table
     * @return
     */
    List<T> findAll();

    /**
     * Limit and offset a set customer from customer table
     * @param limit
     * @param offest
     * @return
     */
    List<T> findAllWithLimitAndOffset(int limit, int offest);

    /**
     * Find customer by using customers_id
     * @param id
     * @return
     */
    T findById(ID id);

    /**
     * Find customer by using their name
     * @param name
     * @return
     */
    T findByName(String name);

    /**
     * Add customer to customer table
     * @param object
     * @return
     */
    int insert(T object);

    /**
     * Update existing customer in customer table
     * @param id
     * @param phone
     * @param email
     * @return
     */
    int update(ID id , String phone , String email);

    /**
     * Return total amount of customers per country
     * @return
     */
    CustomerCountry customersPerCountry();

    /**
     * return most customer spender
     * @return
     */
    CustomerSpender customerSpender();

    /**
     * return the most popular genre by id as parameter
     * @param Customer_id
     * @return
     */
    List<CustomerGenre> customerGenre (int Customer_id);
    int delete(T object);

    /**
     * User can be able to delete any customer in customer table
     * @param id
     * @return
     */
    int deleteById(ID id);
}
