package com.example.springchinook.repository.customer;

import com.example.springchinook.model.Customer;
import com.example.springchinook.repository.CrudRepository;

import java.util.List;

/**
 * Interface class used to achieve abstraction and used to group related methods
 */
public interface CustomerRepository extends CrudRepository<Customer, Integer> {

    String sqlFindAll = "SELECT * FROM customer";
    String sqlFindAllWithLimit = "SELECT * FROM customer LIMIT ? OFFSET ? ";
    String sqlGetById = "SELECT * FROM customer WHERE customer_id = ?";
}
