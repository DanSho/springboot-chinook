package com.example.springchinook.repository.customer;

import com.example.springchinook.model.Customer;
import com.example.springchinook.model.CustomerCountry;
import com.example.springchinook.model.CustomerGenre;
import com.example.springchinook.model.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * a repository anotation is introduced to isolate domain objects from the specifics of the database access code and to reduce the spread and repetition of query code. Basically to access the database
 */
@Repository

/**
 * CustomerRepoImpl implements Customerepo, which basically implements the customerRepo interface
 */
public class CustomerRepositoryImpl implements CustomerRepository{
    /**
     * Fields for connection
     */
    private String url;
    private String username;
    private String password;

    /**
     * properties for connection(connecting with mySQL database)
     * @param url
     * @param username
     * @param password
     */
    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }
   public void sss(){
       String sql = "SELECT * FROM customer";
   }

    /**
     * Returns all customers from customer table. An arraylist is created, followed by that we create an SQL query for finding all customers then we wrap the connection in a try and catch clause.
     * Connecting to the database in line 56
     * Making query and taking the sql as parameter specify that line 55 is a SQL query. And line 58 is executing the sql query
     * Then processing the result and catching exception
     * @return
     */
    @Override
    public List<Customer> findAll() {
        ArrayList<Customer> customers = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
       // String sql = "SELECT * FROM customer";
        try  {
             conn = DriverManager.getConnection(url, username, password);
             ps = conn.prepareStatement(sqlFindAll);
             rs = ps.executeQuery();
            while(rs.next()){
                customers.add(new Customer(
                        rs.getInt("customer_id"),
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getString("country"),
                        rs.getString("postal_code"),
                        rs.getString("phone"),
                        rs.getString("email")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();

        } finally{
            // close JDBC objects
            try {
                if(rs!=null) rs.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            try {
                if(ps!=null) ps.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            try {
                if(conn!=null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return customers;
    }

    /**
     * Returns a set of customers by setting a limit and offset.  An arraylist is created, followed by that we create an SQL query for limit and offset then we wrap the connection in a try and catch clause.
     * Connecting to the database in line 93. In line 95 & 96, we are setting a parameter index for limit and offset. So when we try to print the result user have to enter a number for both placeholders
     * Making query and taking the sql as parameter specify that line 94 is a SQL query. And line 98 is executing the sql query
     * Then processing the result and catching exception
     * @return
     */
    @Override
    public List<Customer> findAllWithLimitAndOffset(int limit, int offset) {
        ArrayList<Customer> customers = new ArrayList<>();
       // String sql = "SELECT * FROM customer LIMIT ? OFFSET ? ";
        Connection conn = null;
        PreparedStatement ps = null;//vi kan skicka SQL-frågor till databasen.
        ResultSet rs = null;//används för att hämta resultat från SQL-valsfrågor
        try {
            conn = DriverManager.getConnection(url, username, password);
             ps = conn.prepareStatement(sqlFindAllWithLimit);
             ps.setInt(1, limit);
             ps.setInt(2, offset);
             rs = ps.executeQuery();
            while(rs.next()){
                customers.add(new Customer(
                        rs.getInt("customer_id"),
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getString("country"),
                        rs.getString("postal_code"),
                        rs.getString("phone"),
                        rs.getString("email")


                ));
            }

        }
        catch (SQLException e) {
            e.printStackTrace();
        }  finally{
            // close JDBC objects
            try {
                if(rs!=null) rs.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            try {
                if(ps!=null) ps.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            try {
                if(conn!=null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return customers;
    }

    /**
     * Returns all customer from database with parameter id.
     * Connecting to the database in line 132
     * Making
     * query and taking the sql as parameter specify that line 133 is a SQL query. And line 135 is executing the sql query
     * Then processing the result and catching exception
     * @return
     */
    @Override
    public Customer findById(Integer id) {
        Customer customer = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
    //  String sql = "SELECT * FROM customer WHERE customer_id = ?";
        try {
            conn = DriverManager.getConnection(url, username, password);
            ps = conn.prepareStatement(sqlGetById);
            ps.setInt(1, id);
            rs = ps.executeQuery();
           while (rs.next()) {
               customer=new Customer(
                       rs.getInt("customer_id"),
                       rs.getString("first_name"),
                       rs.getString("last_name"),
                       rs.getString("country"),
                       rs.getString("postal_code"),
                       rs.getString("phone"),
                       rs.getString("email")


               );

            }
        } catch (SQLException e) {
           e.printStackTrace();
        } finally{
            // close JDBC objects
            try {
                if(rs!=null) rs.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            try {
                if(ps!=null) ps.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            try {
                if(conn!=null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return customer;
    }

    /**
     * Get customers by given firstname from customer table. firstname should be given by user to get full name of customer
     * Connecting to the database in line 167
     * Making query and taking the sql as parameter specify that line 168 is a SQL query. And line 171 is executing the sql query
     * Then processing the result and catching exception
     * @return
     */
    @Override
    public Customer findByName(String name) {
        Customer customer = null;
        String sql = "SELECT * FROM Customer  WHERE first_name = ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);

            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")


                );

            }
            conn.close();
            resultSet.close();
            preparedStatement.close();//slippar leckage i minne och programmet runs snabbare
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    /**
     * Returns added customer with customer object in parameter. Initiating rows-affect to 0, which will later turn to 1 in the output to show affected row. And this is to show that the new added customer has been added to customer table.
     * Connecting to the database in line 203
     * Making query and taking the sql as parameter specify that line 204 is a SQL query. And line 211 is executing the sql query
     * Then processing the result and catching exception
     * @return
     */
    @Override
    public int insert(Customer object) {
        String sql = "INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) VALUES (?,?,?,?,?,?)";
        int rowsAffect = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, object.firstName());
            preparedStatement.setString(2, object.lastName());
            preparedStatement.setString(3, object.country());
            preparedStatement.setString(4, object.postalCode());
            preparedStatement.setString(5, object.phone());
            preparedStatement.setString(6, object.email());
            rowsAffect = preparedStatement.executeUpdate();


        }catch (SQLException e) {
            e.printStackTrace();
        }

        return rowsAffect;


    }


    /**
     * Update existing customer with customer object in parameter.
     * Connecting to the database in line 237
     * Making query and taking the sql as parameter specify that line 240 is a SQL query. And line 244 is executing the sql query
     * Then processing the result and catching exception
     * @return
     */

    @Override
    public int update(Integer id, String phone, String email) {

        String sql = "UPDATE customer SET  phone = ?, email = ? WHERE customer_id= ?";
        int rowsAffect = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            if (!id.equals(id))
                System.out.println(" The Id you entered does not match customer Id");
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, phone);
            preparedStatement.setString(2, email);
            preparedStatement.setInt(3, id);
            rowsAffect = preparedStatement.executeUpdate();

        }catch (SQLException e) {
            e.printStackTrace();
        }

        return rowsAffect;
    }
    /**
     * Shows the total amount of customers per country
     * Connecting to the database in line 265
     * Making query and taking the sql as parameter specify that line 266 is a SQL query. And line 267 is executing the sql query
     * Then processing the result and catching exception
     * @return
     */

    @Override
    public CustomerCountry customersPerCountry() {
        CustomerCountry customersPerCountry = null;
        String sql = "SELECT COUNT(customer_id) as total_customers, country FROM customer " +
                "GROUP BY country ORDER BY COUNT(*) DESC LIMIT 1";
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            PreparedStatement prepp = conn.prepareStatement(sql);
            ResultSet rs = prepp.executeQuery();

            while (rs.next()) {

                customersPerCountry= new CustomerCountry(
                        rs.getString("country"),
                        rs.getInt("total_customers")


                );
            }
        } catch (SQLException e) {
          e.printStackTrace();
            }
        return customersPerCountry;
    }

    /**
     * Shows the total amount of spending per customer
     * Connecting to the database in line 299
     * Making query and taking the sql as parameter specify that line 300 is a SQL query. And line 301 is executing the sql query
     * Then processing the result and catching exception
     * @return
     */
    @Override
    public CustomerSpender customerSpender() {
        CustomerSpender customerSpender = null;
        String sql = "SELECT (customer.customer_id), customer.first_name, customer.last_name, SUM(invoice.total) as total\n" +
                "FROM customer\n" +
                "JOIN invoice ON customer.customer_id = invoice.customer_id GROUP BY invoice.customer_id, customer.customer_id  ORDER by total  desc LIMIT 1";

        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            PreparedStatement prepp = conn.prepareStatement(sql);
            ResultSet rs = prepp.executeQuery();

            while (rs.next()) {

                customerSpender= new CustomerSpender(
                        rs.getInt("customer_id"),
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getDouble("total")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerSpender;
    }

    /**
     * Shows the favourtite genre of a customer by id as parameter.  A list is created, followed by that we create an SQL query for getting the favourite genre of a customer then we wrap the connection in a try and catch clause.
     * Connecting to the database in line 342. In line 344, we are setting a parameter index for customer_id.
     * Making query and taking the sql as parameter specify that line 343 is a SQL query. And line 345 is executing the sql query
     * Then processing the result and catching exception
     * @return
     */
    @Override
    public List<CustomerGenre> customerGenre(int Customer_id) {

        List<CustomerGenre> customerMostPopGenre = new ArrayList<>();

        String sql = "SELECT genre.\"name\", customer.first_name, COUNT(genre.genre_id) AS most_popular_genre\n" +
                "FROM customer\n" +
                "JOIN invoice ON customer.customer_id = invoice.customer_id\n" +
                "JOIN invoice_line ON invoice_line.invoice_id = invoice.invoice_id\n" +
                "JOIN track ON track.track_id = invoice_line.track_id\n" +
                "JOIN genre ON genre.genre_id = track.genre_id\n" +
                "WHERE customer.customer_id = ?\n" +
                "GROUP BY genre.genre_id, genre.\"name\", customer.first_name\n" +
                "ORDER BY most_popular_genre DESC\n" +
                "FETCH FIRST 1 ROWS WITH TIES;";

        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            PreparedStatement prepp = conn.prepareStatement(sql);
            prepp.setInt(1, Customer_id);
            ResultSet rs = prepp.executeQuery();

            while (rs.next()) {

                customerMostPopGenre.add(
                        new CustomerGenre(
                               rs.getString( "name"),
                              rs.getString(  "first_name"),
                              rs.getInt("most_popular_genre" ))
                );
            }

    }

            catch (SQLException e) {
                e.printStackTrace();
            }
            return customerMostPopGenre;
        }





    @Override
    public int delete(Customer object) {
        return 0;
    }
    /**
     * If user wants to delete any row/column in customer table.
     *
     * @return
     */
    @Override
    public int deleteById(Integer id) {
        String sql = "DELETE FROM customer WHERE customer_id= ?";
        int rowsAffect = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            rowsAffect = preparedStatement.executeUpdate();

        }catch (SQLException e) {
            e.printStackTrace();
        }
        return rowsAffect;
    }



}