package com.example.springchinook.runner;

import com.example.springchinook.model.Customer;
import com.example.springchinook.repository.customer.CustomerRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class CustomerRunner implements ApplicationRunner {
    private final CustomerRepository customerRepository;

    public CustomerRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println(customerRepository.findAll());
        System.out.println(customerRepository.findById(2));
        System.out.println(customerRepository.findByName("Daan"));
       // System.out.println(customerRepository.findAllWithLimitAndOffset(4, 2));
       // System.out.println(customerRepository.insert(
          //      new Customer(null, "Hassan", "Mohammed",
            //            "Sweden", "14560", "0743332118",
              //          "hassanmohammed@gmail.com")));
        System.out.println(customerRepository.update(1, "08989332243", "ge9904e@yahoo.com"));
     //   System.out.println(customerRepository.deleteById(68));
      //  System.out.println(customerRepository.customersPerCountry());
      //  System.out.println(customerRepository.customerSpender());
      //  System.out.println(customerRepository.customerGenre(12));




    }
}
